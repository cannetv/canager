<?php
	$active_phase = ($j==0) ? ' active' : '';
	$show_phase   = ($j==0) ? ' show' : '';
	$phase_name   = $phases[$j]['name'];
	$phase_icon   = intval($phases[$j]['type']) === 0 ? 'fa-table' : 'fa-sitemap';
?>

<div 
	class="tab-pane<?php echo $show_phase.$active_phase; ?>" 
	id="pill-<?php echo $i; ?>-<?php echo $j; ?>" 
	role="tabpanel" 
	aria-labelledby="pill-<?php echo $i; ?>-<?php echo $j; ?>-tab">
		<!-- Overview < ? php echo $phase_name; ?> < ? php echo $cat_name; ?> <br/> -->

	<?php 
		if (intval($phases[$j]['type']) === 1)
		{
			include('templates/template-btn-addgroup.php');

			include('templates/template-bracket.php');
		}
		else
		{
			include('templates/template-btn-addgroup.php');
			
			$groups = $phases[$j]['groups'];
			for ($g=0; $g<count($groups); $g++)
			{
				include('templates/template-group-pool.php');
			}
		}

	?>
</div>
