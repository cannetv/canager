<form>
	<div 
		class="form-row"
		cat-id="<?php echo $cats[$i]['uniqid']; ?>" 
		phase-id="<?php echo $phases[$j]['uniqid']; ?>" 
		phase-type="<?php echo $phases[$j]['type']; ?>">

			<button 
				type="button" 
				class="btn btn-primary mr-2 btn-add-group" 
				data-toggle="tooltip" 
				data-placement="top" 
				title="Add a group">
				<i class="fas fa-plus-square"></i>&nbsp;Add
			</button>

			<button 
				type="button" 
				class="btn mr-2 btn-success btn-fill-group" 
				data-toggle="tooltip" 
				data-placement="top" 
				title="Randomly fill the groups with the fighters of the category">
				<i class="fas fa-fill-drip"></i>&nbsp;Fill
			</button>

			<button 
				type="button" 
				class="btn btn-danger btn-empty-group" 
				data-toggle="tooltip" 
				data-placement="top" 
				title="Empty the groups">
				<i class="fas fa-trash-alt"></i>&nbsp;Empty
			</button>

	</div>
</form>

