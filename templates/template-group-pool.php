<?php 
	// Get the current pool
	$pool = $groups[$g];

	/*
	for($i=0; $i<count($pool['matches']); $i++)
	{
		echo '<tr>
				<th scope="row" class="active text-center">'.($i+1).'</th>' . 
			 	'<td>' . $pool['players'][$i]['name'] . '</td>';

		for($j=0; $j<count($pool['grid']); $j++)
		{
			// Cas particuliers
			if ($i == $j)
				echo '<td class="active" id="'.$i.'-'.$j.'"></td>';
			elseif ($pool['grid'][$i][$j] == 666)
				echo '<td class="text-center" id="'.$i.'-'.$j.'">-</td>';
			elseif ($pool['grid'][$j][$i] == 666)
				echo '<td class="text-center" id="'.$i.'-'.$j.'">F</td>';
			else
				echo '<td class="text-center" id="'.$i.'-'.$j.'">'.$pool['grid'][$i][$j].'</td>';
		}
		echo '<td class="text-center"></td><td class="text-center"></td><td class="text-center"></td><td class="text-center"></td><td class="text-center"></td>
			  </tr>';
	}
	*/
?>

<div class="pewl" pewlname="<?php echo $pool['uniqid']; ?>">
	<table class="table-pewl table table-bordered table-condensed">

		<thead>
			<tr>
				<th colspan="<?php echo ($pool['fighters_num']+7); ?>" class="text-center bg-light text-primary"><?php echo $pool['name']; ?></th>
			</tr>

			<tr class="active">
				<th class="text-center">#</th>
				<th>Nom</th>
				<?php
					for ($f=0; $f<$pool['fighters_num']; $f++)
						echo '<th scope="col" class="text-center">'.($f+1).'</th>';
				?>
				<th class="text-center">PV</th>
				<th class="text-center">TD</th>
				<th class="text-center">TR</th>
				<th class="text-center">GA</th>
				<th class="text-center">P</th>
		  	</tr>
		</thead>

		<tbody>

			<?php
				
				// ROWS
				for ($r=0; $r<$pool['fighters_num']; $r++)
				{
					echo '
						<tr>
							<th scope="row" class="active text-center"> '.($r+1).' </th> 
					 		<td><a href="" id="">No fighter yet</a></td>
					';

					// COLUMNS
					for($c=0; $c<$pool['fighters_num']; $c++)
					{
						// Cas particuliers
						if ($r == $c)
							echo '<td class="bg-secondary" id="'.$r.'-'.$c.'"></td>';
						else
						{
							// Check if the match has result
							$res = 0;
							echo '<td class="text-center" id="'.$r.'-'.$c.'">'./*$pool['grid'][$i][$j]*/'.'.'</td>';
						}
							
					}

					echo '
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
							<td class="text-center"></td>
						 </tr>
					';

				}
				
			?>
		</tbody>

	</table>
</div> <!-- div pewl -->