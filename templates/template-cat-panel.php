<?php 
	$cat_name = $cats[$i]['name'];
	$cat_id = $cats[$i]['uniqid'];
	$active = ($i==0) ? ' active' : '';

	$phases = $cats[$i]['phases'];
?>

<div class="tab-pane show<?php echo $active; ?>" id="pills-<?php echo $i; ?>" role="tabpanel" aria-labelledby="pills-<?php echo $i; ?>-tab">

	<div class="container">
		<div class="row">
			<div class="col-2">
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

					<?php 
						for ($j=0; $j<count($phases); $j++)
						{
							include('templates/template-phase-tab.php');
						}

					?>

					<div class="input-group mb-2 mt-2">
					  <input type="text" class="form-control" placeholder="New Phase" aria-label="New Phase" aria-describedby="button-addon2">
					  <div class="input-group-append">
					    <button class="btn btn-secondary btn-add-phase" type="button" id="button-addon2" category="<?php echo $cats[$i]['uniqid']; ?>">
					    	<i class="fas fa-plus-square"></i>
					    </button>
					  </div>
					</div>

					<!--a href="" class="btn-add-phase" category="< ? php echo $cats[$i]['uniqid']; ?>"><i class="fas fa-plus-square text-primary"></i>&nbsp;Add a phase</a-->
				</div>
			</div>
			<div class="col-10">
				<div class="tab-content" id="v-pills-tabContent">

					<?php 
						for ($j=0; $j<count($phases); $j++)
						{
							include('templates/template-phase-panel.php');
						}
					?>
				</div>
			</div>
		</div>
	</div>

</div>
