<?php
	$active_phase = ($j==0) ? ' active' : '';
	$show_phase   = ($j==0) ? ' show' : '';
	$phase_name   = $phases[$j]['name'];
	$phase_id     = $phases[$j]['uniqid'];
	$phase_icon   = intval($phases[$j]['type']) === 0 ? 'fa-table' : 'fa-sitemap';
?>

<a 
	class="nav-link<?php echo $active_phase; ?> btn-rename-phase" 
	id="pill-<?php echo $i; ?>-<?php echo $j; ?>-tab" 
	data-toggle="pill" 
	href="#pill-<?php echo $i; ?>-<?php echo $j; ?>" 
	role="tab" 
	aria-controls="pill-<?php echo $i; ?>-<?php echo $j; ?>" 
	aria-selected="true" 
	phase="<?php echo $phases[$j]['uniqid']; ?>">
		<i class="fas <?php echo $phase_icon; ?>"></i>
		&nbsp;<?php echo $phase_name; ?>
</a>
