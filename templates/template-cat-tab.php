<?php
	$cat_name = $cats[$i]['name'];
	$active = ($i==0) ? ' active' : '';
?>

<li class="nav-item xl-nav-pills">
	<a class="nav-item btn-rename-cat nav-link<?php echo $active; ?>" 
		id="pills-<?php echo $i; ?>-tab" 
		data-toggle="pill" 
		href="#pills-<?php echo $i; ?>" 
		role="tab" 
		aria-controls="pills-<?php echo $i; ?>" 
		aria-selected="true" 
		cat-id="<?php echo $cats[$i]['uniqid']; ?>" 
		>
		<?php 
			echo $cat_name . ' <small>(<i class="fas fa-user"></i>&nbsp;'.count($cats[$i]['fighters']).')</small>';
		?>
	</a>
</li>
