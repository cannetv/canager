<?php
	session_start();

	// Initialize the state
	if (!isset($_SESSION['state']))
	{
		$_SESSION['state'] = 0;
	}

	$fighters = $_SESSION['fighters'];
?>


<!DOCTYPE hmtl>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="A site for canne de combat competition management">
  		<meta name="author" content="Xavier LEJEUNE">

		<title>CANAGER, manage your canne competitions</title>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
		<link rel="stylesheet" href="css/xl-style.css">
		<link rel="stylesheet" href="css/bracket.css">
		
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#">CANAGER</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" id="link-reset" href="">Reset <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="link-todo" href="">TODO</a>
					</li>
				</ul>
			</div>
		</nav>


		<?php
			switch($_SESSION['state'])
			{
				default:
				case 0:
					include('home.php');
				break;

				case 1:
					include('fighters.php');
				break;

				case 2:
					include('competition.php');
				break;
			}
		?>

		<!-- MODALS -->
		<?php 
			include('modals/modal_todo.php'); 
		?>

		<!-- ADDITIONAL SCRIPTS -->
		<script type="text/javascript" src="js/xl-index-functions.js"></script>

	</body>
</html>