$(function(){

	//
	// CHANGE THE DROPDOWN TITLE ACCORDING TO THE CHOICE
	//
	$(".ddi-bracket-updatable").click(function(e){

    	e.preventDefault();

		$(this).parent().parent().find(".btn:first-child").text($(this).text());
		$(this).parent().parent().find(".btn:first-child").val($(this).text());
		$(this).parent().parent().find(".btn:first-child").attr("data-start",$(this).attr("data-start"))

	});

	//
	// WHEN THE "CREATE THE GROUP" BUTTON IS PRESSED
	//
	$("#btn-create-group-bracket").click(function(e) {

		// Get the infos of the window
		var catId = $("#modal-group-bracket").attr("cat-id");
		var phaseId = $("#modal-group-bracket").attr("phase-id");
		var bracketStart = $("#modal-group-bracket").find("#group-bracket-start").attr("data-start");
		var fightAllPlaces = $("#modal-group-bracket").find("#chbx-all-places").prop('checked');

		//alert("Creating from "+bracketStart+" with all places fought : "+fightAllPlaces+" in cat : "+catId+" in phase : "+phaseId);
		
		// Then send the data to the server
		$.post(
			'action.php',
			{ 
				action           : "create_bracket",
				cat_id           : catId,
				phase_id         : phaseId,
				bracket_start    : bracketStart,
				fight_all_places : fightAllPlaces
			},
			function(json) // SUCCES
			{
				if (json.res == "ok")
				{
					// Reload to display the created group
					location.reload();
				}
				else if (json.res == "fail")
				{
					alert("Erreur");
					console.log(json);
				}
			},
			'json'
		).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
			alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
			console.log(jqXHR);
		});
		
	});

});