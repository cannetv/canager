$(function(){

    $('#link-reset').click(function(e){
    	e.preventDefault();

		if (confirm( "Are you sure ? All the competition will be deleted." ))
		{
	    	$.post(
	            'action.php',
	            { action : "reset_competition" },
	            function(json) // SUCCES
	            {
					if (json.res == "ok")
					{
						location.reload();
					}
					else if (json.res == "fail")
					{
						alert("Erreur");
					}
	            },
	            'json'
	        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
	          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
	          console.log(jqXHR);
	        });		    
		}
    });

    // 
    // SHOW THE TODO MODAL
    //
    $('#link-todo').click(function(e) {
    	e.preventDefault();
    	$('#modal-todo').modal('show');
    });

    $('#home-todo-link').click(function(e) {
    	e.preventDefault();
    	$('#modal-todo').modal('show');
    });

	/*
	 * When the user cloads a file, changes the "Choose file..." to the
	 * name of the file, until is changed or uploaded.
	 */
    $('#inputFightersFile').on('change',function(){

        // Get the file name
        var fileName = $(this).val();

        // Remove the unnecessary text
        var cleanFileName = fileName.replace('C:\\fakepath\\', " ")

        // Replace the "Choose a file" label
        $(this).next('.custom-file-label').html(cleanFileName);
    });

    $('#btn-start').click(function(e){
    	e.preventDefault();

    	$.post(
            'action.php',
            { action : "goto_fighters" },
            function(json) // SUCCES
            {
				if (json.res == "ok")
				{
					location.reload();
				}
				else if (json.res == "fail")
				{
					alert("Erreur");
					console.log(json);

				}
            },
            'json'
        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
          console.log(jqXHR);
        });
    });

    $('#btn-start-competition').click(function(e){
    	e.preventDefault();

    	$.post(
            'action.php',
            { action : "goto_competition"},
            function(json) // SUCCES
            {
				if (json.res == "ok")
				{
					location.reload();
				}
				else if (json.res == "fail")
				{
					alert("Erreur");
					console.log(json);
				}
            },
            'json'
        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
          console.log(jqXHR);
        });
    });


    $('#competition-name').click(function(e){
    	e.preventDefault();

    	var competitionName = prompt("Change the name of the competition", $(this).text());
    	competitionName = competitionName.trim();
    	if (competitionName != null)
    	{
	    	$.post(
	            'action.php',
	            { 
	            	action : "name",
	            	name : competitionName
	            },
	            function(json) // SUCCES
	            {
					if (json.res == "ok")
					{
						location.reload();
					}
					else if (json.res == "fail")
					{
						alert("Erreur");
						console.log(json);
					}
	            },
	            'json'
	        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
	          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
	          console.log(jqXHR);
	        });
		} 
    });

    $('#competition-location').click(function(e){
    	e.preventDefault();

    	var competitionLocation = prompt("Change the location of the competition", $(this).text());
    	competitionLocation = competitionLocation.trim();
    	if (competitionLocation != null)
    	{
	    	$.post(
	            'action.php',
	            { 
	            	action : "location",
	            	location : competitionLocation
	            },
	            function(json) // SUCCES
	            {
					if (json.res == "ok")
					{
						location.reload();
					}
					else if (json.res == "fail")
					{
						alert("Erreur");
						console.log(json);
					}
	            },
	            'json'
	        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
	          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
	          console.log(jqXHR);
	        });
		} 
    });

    $('#btn-add-category').click(function(e) {
    	e.preventDefault();

    	var newCategoryName = prompt("Enter a name for the new category", "Category");
    	newCategoryName = newCategoryName.trim();

    	if (newCategoryName != null)
    	{
	    	$.post(
	            'action.php',
	            { 
	            	action : "add_category",
	            	category_name : newCategoryName
	            },
	            function(json) // SUCCES
	            {
					if (json.res == "ok")
					{
						location.reload();
					}
					else if (json.res == "fail")
					{
						alert("Erreur");
						console.log(json);
					}
	            },
	            'json'
	        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
	          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
	          console.log(jqXHR);
	        });
		} 
    });
    
    $('.btn-add-phase').click(function(e) {
    	e.preventDefault();

    	var cat = $(this).attr("category");
    	var newPhaseName = $(this).parent().parent().find("input").val();
    	newPhaseName = newPhaseName.trim();

    	if (newPhaseName != null)
    	{
	    	$.post(
	            'action.php',
	            { 
	            	action : "add_phase",
	            	phase_name : newPhaseName,
	            	category : cat
	            },
	            function(json) // SUCCES
	            {
					if (json.res == "ok")
					{
						location.reload();
					}
					else if (json.res == "fail")
					{
						alert("Erreur");
						console.log(json);
					}
	            },
	            'json'
	        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
	          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
	          console.log(jqXHR);
	        });
		} 
    });

    //
    // Rename a category when the user clicks on the selected category
    //
    $('.btn-rename-cat').click(function(e) {

    	if ($(this).hasClass('active'))
    	{
	    	// Get the category id
	    	var catId = $(this).attr("cat-id");
	    	// Send the id to request informations
	    	$.post(
	            'action.php',
	            { 
	            	action : "get_category_infos",
	            	cat_id : catId
	            },
	            function(json) // SUCCESS
	            {
					if (json.res == "ok")
					{
				    	// Show the modal
				    	$('#modal-category').modal('show');

				    	// Modify the data inside
				    	// Save the category id for later use
				    	$('#modal-category').attr('cat-id', catId);
				    	// Fill the fields
				    	$('#modal-category').find('.modal-body #cat-name').val(json.category.name);
				    	$('#modal-category').find('.modal-body #cat-color').val(json.category.color);
				    	// Add the phases
				    	var itemAddPhase = $('#modal-category').find("#list-item-btn-add-phase");
				    	jQuery.each(json.category.phases, function(i, val) {
				    		$("<li class=\"list-group-item item-phase\">"+ val.name +"</li>").insertBefore(itemAddPhase);
						});
				    	// Add the fighters
				    	jQuery.each(json.category.fighters, function(i, val) {
				    		$('#modal-category').find("#list-fighters").append(
				    			"<span class=\"badge badge-light\">"
				    			+val.last_name+" "+val.first_name+
				    			"</span>&nbsp;"
				    		);
						});
					}
					else if (json.res == "fail")
					{
						alert("Erreur");
						console.log(json);
					}
	            },
	            'json'
	        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
	          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
	          console.log(jqXHR);
	        });
    	}
    });

    //
    // Rename a phase when the user clicks on the selected phase
    //
    $('.btn-rename-phase').click(function(e) {
    	// Check if the button is active
    	if ($(this).hasClass('active'))
    	{
    		// Get the current name
    		currentName = $(this).text();
    		//newName = prompt("Enter the new name for this phase", currentName);
    		categoryId = $(this).parent().find('.btn-add-phase').attr('category');
    		phaseId = $(this).attr('phase');


			// Send the id to request informations
	    	$.post(
	            'action.php',
	            { 
	            	action : "get_phase_infos",
	            	cat_id : categoryId,
	            	phase_id : phaseId
	            },
	            function(json) // SUCCESS
	            {
					if (json.res == "ok")
					{
						var modal = $('#modal-phase');
				    	// Show the modal
				    	modal.modal('show');
				    	// Modify the data inside
				    	// Save the category id for later use
				    	modal.attr('cat-id', categoryId);
				    	modal.attr('phase-id', phaseId);
				    	// Fill the fields
				    	if (json.phase.type === 0) 
				    		modal.find("#radio-phase-pools").prop("checked", true);
				    	else 
				    		modal.find("#radio-phase-bracket").prop("checked", true);

				    	modal.find('.modal-body #phase-name').val(json.phase.name);
				    	modal.find('.modal-body #phase-name').attr('old-name',json.phase.name);
				    	/*
				    	$('#modal-phase').find('.modal-body #cat-color').val(json.category.color);
				    	// Add the phases
				    	var itemAddPhase = $('#modal-phase').find("#list-item-btn-add-phase");
				    	jQuery.each(json.category.phases, function(i, val) {
				    		$("<li class=\"list-group-item\">"+ val.name +"</li>").insertBefore(itemAddPhase);
						});
				    	// Add the fighters
				    	jQuery.each(json.category.fighters, function(i, val) {
				    		$('#modal-category').find("#list-fighters").append(
				    			"<span class=\"badge badge-dark\">"
				    			+val.last_name+" "+val.first_name+
				    			"</span>&nbsp;"
				    		);
						});
*/
					}
					else if (json.res == "fail")
					{
						alert("Erreur");
						console.log(json);
					}
	            },
	            'json'
	        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
	          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
	          console.log(jqXHR);
	        });

    	}
    });
	
	//
	// THE MODAL
	//
    $('#competition-fighters').click(function(e) {
    	e.preventDefault();

    	$('#modal-fighters').modal('show');
    });

	//
	// OPEN THE MODAL GROUP, BUT THE GOOD ONE, 
	// DEPENDING ON THE PHASE TYPE
	//
    $('.btn-add-group').click(function(e) {
    	e.preventDefault();

    	// Get the category id
    	var catId     = $(this).parent().attr('cat-id');
    	var phaseId   = $(this).parent().attr('phase-id');
    	var phaseType = parseInt($(this).parent().attr('phase-type'));

    	if (phaseType == 0)
    	{
    		$('#modal-group-pool').modal('show');
    		$('#modal-group-pool').attr('cat-id', catId);
    		$('#modal-group-pool').attr('phase-id', phaseId);
    	}
    	else if (phaseType == 1)
    	{
			$('#modal-group-bracket').modal('show');
    		$('#modal-group-bracket').attr('cat-id', catId);
    		$('#modal-group-bracket').attr('phase-id', phaseId);
    	}
    	else
    	{
    		alert("ERROR : Wrong phase type, can't decide what to choose");
    	}
    });

	//
	//
    $('.btn-fill-group').click(function(e) {
    	e.preventDefault();

    	// Get the infos (cat id, phase id and phase type) in the button's parent
    	var catId     = $(this).parent().attr('cat-id');
    	var phaseId   = $(this).parent().attr('phase-id');
    	var phaseType = parseInt($(this).parent().attr('phase-type'));

    	alert("Fill those groups");
    });

	//
	//
    $('.btn-empty-group').click(function(e) {
    	e.preventDefault();

    	// Get the infos (cat id, phase id and phase type) in the button's parent
    	var catId     = $(this).parent().attr('cat-id');
    	var phaseId   = $(this).parent().attr('phase-id');
    	var phaseType = parseInt($(this).parent().attr('phase-type'));

    	alert("Empty those groups");
    });


});