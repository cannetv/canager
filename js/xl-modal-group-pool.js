$(function(){

	//
	// CHANGE THE DROPDOWN TITLE ACCORDING TO THE CHOICE
	//
	$(".ddi-updatable").click(function(e){

    	e.preventDefault();

		$(this).parent().parent().find(".btn:first-child").text($(this).text());
		$(this).parent().parent().find(".btn:first-child").val($(this).text());

	});

	//
	// WHEN THE "CREATE THE GROUP" BUTTON IS PRESSED
	//
	$("#btn-create-group-pool").click(function(e) {

		// Get the infos of the window
		var catId = $("#modal-group-pool").attr("cat-id");
		var phaseId = $("#modal-group-pool").attr("phase-id");
		var groupName = $("#modal-group-pool").find("#group-pool-name").val();
		var fightersNumber = $("#modal-group-pool").find("#group-pool-fighters").text();
		var poolType = $("#modal-group-pool").find("#group-pool-type").text();
		
		// Then send the data to the server
		$.post(
			'action.php',
			{ 
				action       : "create_group_pool",
				cat_id       : catId,
				phase_id     : phaseId,
				group_name   : groupName,
				fighters_num : fightersNumber,
				pool_type    : poolType
			},
			function(json) // SUCCES
			{
				if (json.res == "ok")
				{
					// Reload to display the created group
					location.reload();
				}
				else if (json.res == "fail")
				{
					alert("Erreur");
					console.log(json);
				}
			},
			'json'
		).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
			alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
			console.log(jqXHR);
		});
	});

});