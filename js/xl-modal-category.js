$(document).ready(function() {

	//
	// When the user chooses a color in the dropdown, it colors the button with this color
	//
    $('.ddi-color').click(function(e) {
    	e.preventDefault();

    	color = $(this).data("color");
    	$(this).parent().parent().find('.dropdown-toggle').removeClass("btn-primary btn-success btn-danger btn-warning btn-info").addClass("btn-"+color);
    });

    // 
    // WHEN THE USER SAVES THE DATA
    //
    // TODO : use focusout to rename the category ?
    //
    $('#btn-modal-cat-save-data').click(function(e) {
    	e.preventDefault();

    	var newName = $('#modal-category').find('.modal-body #cat-name').val();
		var newColor = $('#modal-category').find('.modal-body #cat-color').val();

    	if (newName != null)
    	{
    		catId = $('#modal-category').attr("cat-id");
	    	$.post(
	            'action.php',
	            { 
	            	action : "change_cat_name",
	            	new_name : newName,
	            	cat_id : catId
	            },
	            function(json) // SUCCES
	            {
					if (json.res == "ok")
					{
						$('#modal-category').modal('hide');
						location.reload();
					}
					else if (json.res == "fail")
					{
						alert("Erreur");
						console.log(json);
					}
	            },
	            'json'
	        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
	          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
	          console.log(jqXHR);
	        });
		} 
    });

    //
    // WHEN THE USER ADDS A NEW PHASE
    //
	$('#btn-add-phase').click(function(e) {
    	e.preventDefault();

		// Get the input phase name and the ategory
    	var cat = $("#modal-category").attr("cat-id");
    	var newPhaseName = $("#modal-category").find("#input-phase-name").val();
    	newPhaseName = newPhaseName.trim();

    	if (newPhaseName != null)
    	{
	    	$.post(
	            'action.php',
	            { 
	            	action : "add_phase",
	            	phase_name : newPhaseName,
	            	category : cat
	            },
	            function(json) // SUCCES
	            {
					if (json.res == "ok")
					{
						//location.reload();
						// Manually add the phase in the list (because if we reload, it will quit the the window)
						$("#modal-category").find("#input-phase-name").val("");
						var itemAddPhase = $('#modal-category').find("#list-item-btn-add-phase");
						$("<li class=\"list-group-item item-phase\">"+ newPhaseName +"</li>").insertBefore(itemAddPhase);
					}
					else if (json.res == "fail")
					{
						alert("Erreur");
						console.log(json);
					}
	            },
	            'json'
	        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
	          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
	          console.log(jqXHR);
	        });
		} 
		
	});

    //
    // WHEN THE MODAL IS CLOSING, EMPTY THE LISTS
    //
	$('#modal-category').on('hidden.bs.modal', function () {
		$(".list-group li").remove(".item-phase");
		$(this).find("#list-fighters").empty();
	});

});