$(document).ready(function() {

    // 
    // WHEN THE USER SAVES THE DATA
    //
    $('#btn-modal-phase-save-data').click(function(e) {
    	e.preventDefault();

    	var newName = $('#modal-phase').find('.modal-body #phase-name').val();

    	if (newName != null)
    	{
    		phaseId = $('#modal-phase').attr("phase-id");
    		categoryId = $('#modal-phase').attr("cat-id");

	    	$.post(
	            'action.php',
	            { 
	            	action : "change_phase_name",
	            	new_name : newName,
	            	phase_id : phaseId,
	            	cat_id   : categoryId
	            },
	            function(json) // SUCCES
	            {
					if (json.res == "ok")
					{
						$('#modal-phase').modal('hide');
						location.reload();
					}
					else if (json.res == "fail")
					{
						alert("Erreur");
						console.log(json);
					}
	            },
	            'json'
	        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
	          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
	          console.log(jqXHR);
	        });

		} 
    });

    //
    // WHEN THE FIELD NAME LOSES THE FOCUS
    //
    $('#phase-name').focusout(function() {
    	// Get the potential new name
    	newName = $(this).val().trim();
    	// Get the old name for comparison
    	oldName = $(this).attr('old-name');

    	// If the names are different, commit to server
    	if (newName != oldName && newName != "")
    	{
    		phaseId = $('#modal-phase').attr("phase-id");
    		categoryId = $('#modal-phase').attr("cat-id");

	    	$.post(
	            'action.php',
	            { 
	            	action : "change_phase_name",
	            	new_name : newName,
	            	phase_id : phaseId,
	            	cat_id   : categoryId
	            },
	            function(json) // SUCCES
	            {
					if (json.res == "ok")
					{
						//$('#modal-phase').modal('hide');
						// Change the old-name field
						$(this).attr('old-name', newName);
					}
					else if (json.res == "fail")
					{
						alert("Erreur");
						console.log(json);
					}
	            },
	            'json'
	        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
	          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
	          console.log(jqXHR);
	        });
    	}

  	});

	//
	// WHEN THE RADIO BUTTONS ARE CHANGING
	//
	$('.input-phase-type').change(function() {

	    var type = ($(this).attr('value') == 'pools') ? "0" : "1";

		phaseId = $('#modal-phase').attr("phase-id");
		categoryId = $('#modal-phase').attr("cat-id");

    	$.post(
            'action.php',
            { 
            	action : "change_phase_type",
            	new_type : type,
            	phase_id : phaseId,
            	cat_id   : categoryId
            },
            function(json) // SUCCES
            {
				if (json.res == "ok")
				{
				}
				else if (json.res == "fail")
				{
					alert("Erreur");
					console.log(json);
				}
            },
            'json'
        ).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
          alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
          console.log(jqXHR);
        });


	});

    //
    // WHEN THE MODAL IS CLOSING
    //
	$('#modal-phase').on('hidden.bs.modal', function () {
		// Empty the lists
		// And reload the page
		location.reload();
	});
});