$(function(){

	// 
	// WHAT HAPPENS WHEN THE CHECKBOX IS CLICKED
	//
	$('#chbx-hide-fighters').click(function(e) {
		var hideFighters = $(this).is(':checked') ? true : false;
		var val = hideFighters ? '1' : '0';
		$.post(
			'action.php',
			{ 
				action : "hide_f_in_cat",
				value : val
			},
			function(json) // SUCCES
			{
				if (json.res == "ok")
				{
					// Do not reload, wait for the user to close the modal
					//location.reload();
					// Hide all the checked ones
					if (hideFighters)
					{
						$('.chbx-cat').each(function() {
							if ($(this).is(':checked'))
								$(this).parent().parent().parent().hide();
						});
					}
					// Reveal all the lines
					else
						$('.chbx-cat').parent().parent().parent().show();
				}
				else if (json.res == "fail")
				{
					alert("Erreur");
					console.log(json);
				}
			},
			'json'
		).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
			alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
			console.log(jqXHR);
		});
	});

    //
    // WHAT HAPPENS WHEN THE USER CHOOSES A CATEGORY FOR A FIGHTER
    // 
	$('.chbx-cat').click(function(e) {
		var cat = $(this).parent().attr('category');
		var fighter = $(this).parent().attr('fighter');
		var actionType = $(this).is(':checked') ? 'add_f_to_cat' : 'remove_f_from_cat';
		var thisElement = $(this);

		$.post(
			'action.php',
			{ 
				action : actionType,
				fighter_id : fighter,
				cat_id : cat
			},
			function(json) // SUCCES
			{
				if (json.res == "ok")
				{
					// Do not reload, wait for the user to close the modal
					//location.reload();
					// Hide the line if needed
					var hideFighters = $('#chbx-hide-fighters').is(':checked');
					if (hideFighters)
						thisElement.parent().parent().parent().hide();
				}
				else if (json.res == "fail")
				{
					alert("Erreur");
					console.log(json);
				}
			},
			'json'
		).fail(function(jqXHR, textStatus, errorThrown) { // ECHEC
			alert( "Error in POST data : "+errorThrown + " Status : " + textStatus + " jqXHR : " + jqXHR);
			console.log(jqXHR);
		});
	});

	//
	// WHEN WE OPEN THE MODAL, HIDE THE CHECKED LINES IF THE OPTION IS SELECTED
	//
	$('#modal-fighters').on('show.bs.modal', function (e) {
  		var hideFighters = $('#chbx-hide-fighters').is(':checked');
  		if (hideFighters)
  		{
			$('.chbx-cat').each(function() {
				if ($(this).is(':checked'))
					$(this).parent().parent().parent().hide();
			});
		}

	});

	//
	// WHEN WE CLOSE THE MODAL, RELOAD THE PAGE
	//
	$('#modal-fighters').on('hidden.bs.modal', function (e) {
  		location.reload();
	});

});