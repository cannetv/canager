<?php
	session_start();

	$_SESSION['state'] = 2;

	$_SESSION['competition'] = array(
		"name"       => "Competition name",
		"dates"      => date("j/n/Y") . " to " . date("j/n/Y"),
		"location"   => "Location",
		"categories" => array(
			0 => array(
				"name"     => "Category 1",
				"fighters" => array(),
				"matches"  => array()
			)
		)
	);

	echo json_encode(array(
		"res" => "ok"
	));

?>