<?php
	header("Access-Control-Allow-Origin: *");
	header("Content-type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Max-Age: 3600");
	header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

	session_start();


	if(isset($_POST['competition_name']))
	{
		$name = trim(strip_tags($_POST['competition_name']));

		if (!empty($name))
		{
			$_SESSION['competition']['name'] = $name;
			echo json_encode(array(
				"res" => "ok"
			));
		}
		else
		{
			echo json_encode(array(
				"res" => "fail",
				"errors" => array(
					0 => "The given name is empty or not valid. Please try again."
				)
			));			
		}
	
	}
	else
	{
		echo json_encode(array(
			"res" => "fail",
			"errors" => array(
				0 => "Name is missing, please retry your request."
			)
		));
	}

?>