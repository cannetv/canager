<?php
	$cats = $_SESSION['competition']['categories'];

?>

<div class="container mt-3">
	<h1>THE COMPETITION</h1>
</div>

<div class="container mb-3">
	<div class="card">
		<div class="card-body">
			<h5 class="card-title">Global informations</h5>
			<ul>
				<li>
					NAME : 
					<!-- On one line because else we have uselesse blank spaces in the "prompt" window -->
					<a href="" id="competition-name" data-toggle="tooltip" data-placement="right" title="Click to change the name"><?php echo $_SESSION['competition']['name']; ?></a>
				</li>
				<li>DATES : <?php echo $_SESSION['competition']['dates']; ?></li>
				<li>
					LOCATION : 
					<!-- Same problem -->
					<a href="" id="competition-location" data-toggle="tooltip" data-placement="right" title="Click to change the location"><?php echo $_SESSION['competition']['location']; ?></a>
				</li>
				<li>
					FIGHTERS : 
					<a href="" id="competition-fighters" data-toggle="tooltip" data-placement="right" title="Click to manage the fighters">
						<?php echo count($_SESSION['fighters']); ?>&nbsp;participants
					</a>
				</li>
			</ul>
		</div>
	</div>

</div>

<div class="container">

	<div class="row">

		<div class="col-10">
			<ul class="nav nav-pills nav-justified mb-3" id="pills-tab" role="tablist">
				<?php
					for ($i=0; $i<count($cats); $i++)
					{
						include('templates/template-cat-tab.php');
					}
				?>
			</ul>
		</div>

		<div class="col-2">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="New Category" aria-label="New Category" aria-describedby="btn-add-category">
				<div class="input-group-append">
					<button class="btn btn-secondary " type="button" id="btn-add-category" data-toggle="tooltip" data-placement="right" title="Add a category">
						<i class="fas fa-plus-square"></i>
					</button>
				</div>
			</div>
			<!--button type="button" class="btn btn-secondary btn-lg" id="btn-add-category" data-toggle="tooltip" data-placement="right" title="Add a category">
				<i class="fas fa-plus-square"></i>
			</button-->
		</div>

	</div> <!-- row -->

</div> <!-- container -->


<!-- 
	TODO : change pills color : 
	- https://stackoverflow.com/questions/48927089/bootstrap-4-change-color-of-nav-pill-active?rq=1
	- https://stackoverflow.com/questions/10560786/changing-color-of-twitter-bootstrap-nav-pills
-->

<!--
	TODO : for brackets
	- https://blog.codepen.io/2018/02/16/need-make-tournament-bracket/ 
--> 

<div class="tab-content" id="pills-tabContent">
	<?php
		for ($i=0; $i<count($cats); $i++)
		{
			include('templates/template-cat-panel.php');
		}
	?>

</div>

<!-- MODALS -->
<?php 

	include('modals/modal_fighter_to_category.php');
	include('modals/modal_category.php'); 
	include('modals/modal_phase.php'); 
	include('modals/modal_group_pool.php'); 
	include('modals/modal_group_bracket.php'); 

?>