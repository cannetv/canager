<div class="modal fade" id="modal-fighters" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Affect categories to fighters</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-check">
					<?php
						$checked = $_SESSION['hide_fighters_in_category'] ? ' checked' : '';
						echo '<input class="form-check-input" type="checkbox" value="" id="chbx-hide-fighters"'.$checked.'>';
					?>
					<label class="form-check-label" for="chbx-hide-fighters">
						Hide fighters who are at least in one category
					</label>
				</div>
				<div class="container-fluid">
					
			        <?php
			    		if (isset($fighters))
						{
							for ($i=0; $i<count($fighters); $i++)
							{
								$f = $fighters[$i];

								$choices_categories = '';
								for ($j=0; $j<count($cats); $j++)
								{
									$checked = '';
									foreach($cats[$j]['fighters'] as $ff)
									{
										if ($ff['uniqid'] === $f['uniqid'])
										{
											$checked = ' checked';
											break;
										}
									}
									$choices_categories .= '
										<span class="form-check form-check-inline" category="'.$cats[$j]['uniqid'].'" fighter="'.$f['uniqid'].'">
											<input class="form-check-input chbx-cat" type="checkbox" value="" id="defaultCheck'.$j.'-'.$i.'" '.$checked.'>
											<label class="form-check-label" for="defaultCheck'.$j.'-'.$i.'">'.
												$cats[$j]['name'] .'
											</label>
										</span>
									';
								}

								$gender = '';
								if (trim($f['gender']) == 'm') $gender .= '<i class="fas fa-mars text-danger"></i>';
								else $gender .= '<i class="fas fa-venus text-primary"></i>';
								$line = '
									<div class="row">
										<div class="col-4">'.$gender.'&nbsp;' . $f['last_name']." ".$f['first_name']. '</div>
										<div class="col-8">'.$choices_categories.'</div>
									</div>
								';

								echo $line;
							}
						}
						else echo "No fighter loaded.";
			        ?>
				</div> <!-- container -->
	      	</div>
			<div class="modal-footer">
				<!--button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button-->
				<button type="button" class="btn btn-primary" data-dismiss="modal">Terminé</button>
			</div>
		</div>
	</div>
</div>

<!-- Add the script that work with it -->
<script type="text/javascript" src="js/xl-modal-fighters.js"></script>