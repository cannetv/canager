<div class="modal fade" id="modal-phase" tabindex="-1" role="dialog" aria-labelledby="modal-phase-label" aria-hidden="true" cat-id="" phase-id="">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <!-- HEADER -->
      <div class="modal-header">
        <h5 class="modal-title" id="modal-phase-label">Phase Parameters</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <!-- BODY -->
      <div class="modal-body">
        <form>

          <div class="form-group row">
            <label for="phase-name" class="col-form-label col-sm-2">Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="phase-name" old-name="">
            </div>
          </div>

          <div class="form-group row">
            <label for="phase-name" class="col-form-label col-sm-2">Type</label>
            <div class="col-sm-10">
              <div class="form-check form-check-inline">
                <input class="form-check-input input-phase-type" type="radio" name="inlineRadioOptions" id="radio-phase-pools" value="pools">
                <label class="form-check-label" for="radio-phase-pools">Pools</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input input-phase-type" type="radio" name="inlineRadioOptions" id="radio-phase-bracket" value="bracket">
                <label class="form-check-label" for="radio-phase-bracket">Bracket</label>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="message-text" class="col-form-label">Groupes</label>
            <ul class="list-group" id="list-groups">
              <li class="list-group-item list-group-item-secondary" id="list-item-btn-add-group">
                <a href="" class="btn-add-group">
                  <i class="fas fa-plus-square text-primary"></i>&nbsp;Add a group
                </a>
              </li>
            </ul>
          </div>

        </form>
      </div> <!-- modal-body -->

      <!-- FOOTER -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-modal-phase-save-data">Save changes</button>
      </div>

    </div>
  </div>
</div>

<!-- Add the script that work with it -->
<script type="text/javascript" src="js/xl-modal-phase.js"></script>