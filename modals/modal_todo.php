<?php
	$todo = array(
		"Modify the name of a group",
		"Fill pools and brackets with fighters from the category (only)",
		"Allow to change the dates",
		"Manage to go back to the current tab when the server receives an information, so that when the page is reloaded, you don't have to click several times to go back where you were",
		"Generate a planification of the matches once phases and groups created and filled",
		"Save the data locally (local storage ?) and on the server to be able to work even disconnected or if the browser crashes and needs to be reloaded",
		"Export all the results and the data (csv, json ?)",
		"Link to Cannelo"
	);

	$in_progress = array(
		"Create groups",
		"Modal windows to handle categories (name, phases, fighters, color) and phases (name, groups)",
		"Delete categories, phases and groups"
	);

	$done = array(
		"Have pools phases and bracket phases",
		"Load a list of fighters (first name, last name, licence, birth date)",
		"Create categories and phases",
		"Modify the name of the categories and of the phases",
		"BUG : replace names by uniqid where the search is needed",
		"Put fighters into categories",
		"Add a TODO modal with features to do (red), in progress (orange) and done (green)",
		"Add a type (pools or bracket) to the phases and handle it",
		"Make the \"Hide fighters who are at least in one category\" checkbox actually work",
		"Add the number of fighters in a category in the category tab",
		"Replace the procedural names of the phases and categories in the tabs by an index"
	);
?>


<div class="modal fade" id="modal-todo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">TODO : tasks done, in progress and yet to be done</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<ol>
					<?php
						foreach($done as $d)
							echo '<li><div class="alert alert-success" role="alert">'.$d.'</div></li>';

						foreach($in_progress as $d)
							echo '<li><div class="alert alert-warning" role="alert">'.$d.'</div></li>';

						foreach($todo as $d)
							echo '<li><div class="alert alert-danger" role="alert">'.$d.'</div></li>';
					?>
				</ol>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>