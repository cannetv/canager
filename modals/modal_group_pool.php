<!-- 
  MODAL GROUP POOL
 -->
<div class="modal fade" id="modal-group-pool" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" cat-id="" phase-id="">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Group - pool</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form>

          <div class="form-group row">
            <label for="group-pool-name" class="col-form-label col-sm-3">Name</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="group-pool-name">
            </div>
          </div>

          <div class="form-group row">
            <label for="group-pool-fighters" class="col-form-label col-sm-3">Fighters num</label>
            <div class="btn-group col-sm-9">
              <button type="button" class="btn btn-primary dropdown-toggle" id="group-pool-fighters" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Choose
              </button>
              <div class="dropdown-menu">
                <a class="dropdown-item ddi-updatable" href="">3</a>
                <a class="dropdown-item ddi-updatable" href="">4</a>
                <a class="dropdown-item ddi-updatable" href="">5</a>
                <a class="dropdown-item ddi-updatable" href="">6</a>
                <a class="dropdown-item ddi-updatable" href="">7</a>
                <a class="dropdown-item ddi-updatable" href="">8</a>
                <a class="dropdown-item ddi-updatable" href="">9</a>
                <a class="dropdown-item ddi-updatable" href="">10</a>
              </div>
            </div>
          </div>

          <div class="form-group row">
            <label for="group-pool-type" class="col-form-label col-sm-3">Mode</label>
            <div class="btn-group col-sm-9">
              <button type="button" class="btn btn-primary dropdown-toggle" id="group-pool-type" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Choose
              </button>
              <div class="dropdown-menu">
                <a class="dropdown-item ddi-updatable" href="">Round robin</a>
                <a class="dropdown-item ddi-updatable" href="">Circular pool</a>
              </div>
            </div>
          </div>

        </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="btn-create-group-pool">Create the group</button>
      </div>

    </div>
  </div>
</div>

<!-- Add the script that work with it -->
<script type="text/javascript" src="js/xl-modal-group-pool.js"></script>