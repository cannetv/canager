<!-- 
  MODAL GROUP POOL
 -->
<div class="modal fade" id="modal-group-bracket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" cat-id="" phase-id="">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Group - bracket</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form>

          <div class="form-group row">
            <label for="group-pool-name" class="col-form-label col-sm-3">Starting</label>
            <div class="btn-group col-sm-9">
              <button type="button" class="btn btn-primary dropdown-toggle" id="group-bracket-start" data-start="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Choose
              </button>
              <div class="dropdown-menu">
                <a class="dropdown-item ddi-bracket-updatable" href="" data-start="8">1/8 of finals</a>
                <a class="dropdown-item ddi-bracket-updatable" href="" data-start="4">1/4 of finals</a>
                <a class="dropdown-item ddi-bracket-updatable" href="" data-start="2">1/2 finals</a>
              </div>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-12">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="chbx-all-places">
                <label class="form-check-label" for="chbx-all-places">
                  Fight all the places
                </label>
              </div>
            </div>
          </div>

        </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="btn-create-group-bracket">Create the bracket</button>
      </div>

    </div>
  </div>
</div>

<!-- Add the script that work with it -->
<script type="text/javascript" src="js/xl-modal-group-bracket.js"></script>