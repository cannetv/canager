<div class="modal fade" id="modal-category" tabindex="-1" role="dialog" aria-labelledby="modal-category-label" aria-hidden="true" cat-id="">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <!-- HEADER -->
      <div class="modal-header">
        <h5 class="modal-title" id="modal-category-label">Category parameters</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <!-- BODY -->
      <div class="modal-body">
        <form>

          <div class="form-group row">
            <label for="cat-name" class="col-form-label col-sm-2">Name</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="cat-name">
            </div>
            <div class="btn-group col-sm-3">
              <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Color
              </button>
              <div class="dropdown-menu">
                <a class="dropdown-item text-primary ddi-color" data-color="primary" href="">Color 1</a>
                <a class="dropdown-item text-success ddi-color" data-color="success" href="">Color 2</a>
                <a class="dropdown-item text-danger ddi-color" data-color="danger" href="">Color 3</a>
                <a class="dropdown-item text-warning ddi-color" data-color="warning" href="">Color 4</a>
                <a class="dropdown-item text-info ddi-color" data-color="info" href="">Color 5</a>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="message-text" class="col-form-label">Phases</label>
            <ul class="list-group" id="list-phases">

                <li class="list-group-item list-group-item-secondary" id="list-item-btn-add-phase">
                  <div class="input-group mb-3">
                    <input type="text" class="form-control" id="input-phase-name" placeholder="New phase name" aria-label="New phase name" aria-describedby="btn-add-phase">
                    <div class="input-group-append">
                      <button class="btn btn-secondary" type="button" id="btn-add-phase"><i class="fas fa-plus-square"></i>&nbsp;Add a phase</button>
                    </div>
                  </div>
                </li>

              <!--li class="list-group-item list-group-item-secondary" id="list-item-btn-add-phase">
                <a href="" class="btn-add-phase" category="'.$cats[$i]['uniqid'].'">
                  <i class="fas fa-plus-square text-primary"></i>&nbsp;Add a phase
                </a>
              </li-->
            </ul>
          </div>

          <div class="form-group">
            <label for="cat-fighters" class="col-form-label">Fighters</label>
            <div id="list-fighters">
            </div>
          </div>
        </form>
      </div>

      <!-- FOOTER -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-modal-cat-save-data">Save changes</button>
      </div>

    </div>
  </div>
</div>

<!-- Add the script that work with it -->
<script type="text/javascript" src="js/xl-modal-category.js"></script>