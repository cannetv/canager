<?php
	header("Access-Control-Allow-Origin: *");
	header("Content-type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Max-Age: 3600");
	header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

	define('POOL', 0);
	define('BRACKET', 1);

	session_start();


	if(isset($_POST['action']) && !empty($_POST['action'])) {
		$action = $_POST['action'];
		switch($action)
		{
			case 'goto_fighters'      : goto_fighters(); break;
			case 'goto_competition'   : goto_competition(); break;
			case 'name'               : change_competition_info($action); break;
			case 'location'           : change_competition_info($action); break;
			case 'date_1'             : change_competition_info($action); break;
			case 'date_2'             : change_competition_info($action); break;
			case 'add_category'       : add_category(); break;
			case 'add_phase'          : add_phase(); break;
			case 'get_category_infos' : get_category_infos(); break;
			case 'change_cat_name'    : change_cat_name(); break;
			case 'get_phase_infos'    : get_phase_infos(); break;
			case 'change_phase_name'  : change_phase_name(); break;
			case 'change_phase_type'  : change_phase_type(); break;
			case 'hide_f_in_cat'      : hide_fighters_in_category(); break; 
			case 'add_f_to_cat'       : add_fighter_to_category(); break;
			case 'remove_f_from_cat'  : remove_fighter_from_category(); break;
			case 'reset_competition'  : reset_competition(); break;
			case 'create_group_pool'  : create_group_pool(); break;
			case 'create_bracket'     : create_bracket(); break;
			case 'fill_groups'        : fill_groups(); break;
			case 'empty_groups'       : empty_groups(); break;
			default : 
				echo json_encode(array(
					"res" => "fail",
					"errors" => array(
						0 => "Wrong action name."
					)
				));
		}
		// Then save the data
	}


	/***********************************************************************/
	/***																 ***/
	/***                   			FUNCTIONS  							 ***/
	/***																 ***/
	/***********************************************************************/

	//
	// MOVE TO THE FIGHTERS STATE
	//
	function goto_fighters()
	{
		$_SESSION['state'] = 1;

		echo json_encode(array("res" => "ok"));
	}

	// 
	// MOVE TO THE COMPETITION STATE
	//
	function goto_competition()
	{
		$_SESSION['state'] = 2;

		$_SESSION['competition'] = array(
			"name"       => "Competition name",
			"dates"      => date("j/n/Y") . " to " . date("j/n/Y"),
			"location"   => "Location",
			"categories" => array(
				0 => create_category()
			)
		);

		echo json_encode(array("res" => "ok"));
	}

	//
	// RE-INITIALIZE THE COMPETITION
	//
	function reset_competition()
	{
		$_SESSION['state'] = 0;

		$_SESSION['competition'] = null;
		$_SESSION['fighters'] = null;

		echo json_encode(array("res" => "ok"));
	}

	//
	// CHANGE THE NAME/LOCATION/DATE OF THE COMPETITION
	//
	function change_competition_info($type)
	{		
		if(isset($_POST[$type]))
		{
			$info = trim(strip_tags($_POST[$type]));

			if (!empty($info))
			{
				$_SESSION['competition'][$type] = $info;
				echo json_encode(array("res" => "ok"));
			}
			else json_fail("The given " . $type . " is empty or not valid. Please try again.");			
		
		}
		else json_fail($type . " is missing, please retry your request.");
	}

	// 
	// CREATE AND RETURN A NEW CATEGORY 
	// (AND THUS A PHASE AND A GROUP INSIDE THE PHASE)
	//
	function create_category($cat_name = "new Category")
	{
		return array(
					"name"     => $cat_name,
					"uniqid"   => uniqid("c_"),
					"color"    => "primary",
					"fighters" => array(),
					"phases"   => array(
						0 => create_phase()
					)
				);
	}

	// 
	// CREATE AND RETURN A NEW PHASE
	// (AND THUS A GROUP)
	//
	function create_phase($phase_name = "new Phase")
	{
		return array(
					"name"   => $phase_name,
					"uniqid" => uniqid("p_"),
					"type"   => POOL,
					"groups" => array(
						//0 => create_group()
					)
				);
	}

	// 
	// CREATE AND RETURN A NEW GROUP
	//
	function create_group($group_name = "new Group", $fighters_num = 4)
	{
		return array(
					"name"         => $group_name,
					"uniqid"       => uniqid("g_"),
					"fighters_num" => $fighters_num,
					"fighters"     => array(),
					"matches"      => array()
				);
	}

	//
	// CREATE AND RETURN A NEW MATCH
	//
	function create_match($i, $j)
	{
		return array(
			"id1" => $i,
			"id2" => $j,
			"score1" => -1,
			"score2" => -1
		);
	}

	//
	// GET A CATEGORY BY REFERENCE (DO NOT FORGET THE '&')
	//
	function &get_category($cat_id)
	{
		for ($i=0; $i<count($_SESSION['competition']['categories']); $i++)
		{
			if ($_SESSION['competition']['categories'][$i]['uniqid'] === $cat_id)
				return $_SESSION['competition']['categories'][$i];
		}
		return null;
	}

	//
	// GET A PHASE BY REFERENCE, KNOWING THE CATEGORY (DO NOT FORGET THE '&s')
	//
	function &get_phase($phase_id, &$cat)
	{
		for ($i=0; $i<count($cat['phases']); $i++)
		{
			if ($cat['phases'][$i]['uniqid'] === $phase_id)
				return $cat['phases'][$i];
		}
		return null;
	}

	// 
	// ADD A CATEGORY
	//
	function add_category()
	{
		if(isset($_POST['category_name']))
		{
			$cat_name = trim(strip_tags($_POST['category_name']));

			if (!empty($cat_name))
			{
				$new_cat = create_category($cat_name);

				array_push($_SESSION['competition']['categories'], $new_cat);
				
				echo json_encode(array("res" => "ok"));
			}
			else json_fail("The given category name is empty or not valid. Please try again.");		
		}
		else json_fail("Category name is missing, please retry your request.");
	}

	//
	// FIND A CATEGORY AND RETURN ITS VALUES
	//
	function get_category_infos()
	{
		$cat_id = trim(strip_tags($_POST['cat_id']));
		if(isset($cat_id))
		{
			// Find the category
			$cat = &get_category($cat_id);
			/*
			for ($i=0; $i<count($_SESSION['competition']['categories']); $i++)
			{
				if ($_SESSION['competition']['categories'][$i]['uniqid'] === $cat_id)
				{
					$cat = $_SESSION['competition']['categories'][$i];
					break;
				}
			}
			*/
			if ($cat !== null)
			{
				echo json_encode(array(
					"res" => "ok",
					"category" => array(
						"name"  => $cat['name'],
						"color" => $cat['color'],
						"fighters" => $cat['fighters'],
						"phases" => $cat['phases']
					)
				));
			}
			else json_fail("Category ".$cat_id." not found.");
		}
		else json_fail("Category id is missing, please retry your request.");
	}

	//
	// CHANGE THE NAME OF A CATEGORY
	//
	function change_cat_name()
	{
		$cat_id = trim(strip_tags($_POST['cat_id']));
		$new_name = trim(strip_tags($_POST['new_name']));
		if(isset($cat_id) && isset($new_name))
		{
			// Find the category
			// To use get_category(), it has to send the ategory reference, not a copy (because we change the name)
			$categoryFound = false;
			for ($i=0; $i<count($_SESSION['competition']['categories']); $i++)
			{
				if ($_SESSION['competition']['categories'][$i]['uniqid'] === $cat_id)
				{
					$_SESSION['competition']['categories'][$i]['name'] = $new_name;
					$categoryFound = true;
					break;
				}
			}
			if ($categoryFound) echo json_encode(array("res" => "ok"));
			else json_fail("Category ".$cat_id." not found.");

		}
		else json_fail("At least one name is missing");
	}

	//
	// FIND A PHASE AND RETURN ITS VALUES
	//
	function get_phase_infos()
	{
		$cat_id = trim(strip_tags($_POST['cat_id']));
		$phase_id = trim(strip_tags($_POST['phase_id']));
		if(isset($cat_id) && isset($phase_id))
		{
			// Find the category
			$cat = &get_category($cat_id);
			/*
			for ($i=0; $i<count($_SESSION['competition']['categories']); $i++)
			{
				if ($_SESSION['competition']['categories'][$i]['uniqid'] === $cat_id)
				{
					$cat = $_SESSION['competition']['categories'][$i];
					break;
				}
			}
			*/
			if ($cat !== null)
			{
				// Find the phase
				$phase = &get_phase($phase_id, $cat);
				/*
				for ($i=0; $i<count($cat['phases']); $i++)
				{
					if ($cat['phases'][$i]['uniqid'] === $phase_id)
					{
						$phase = $cat['phases'][$i];
						break;
					}
				}
				*/
				if ($phase != null)
				{
					echo json_encode(array(
						"res" => "ok",
						"phase" => array(
							"name"  => $phase['name'],
							"type"  => $phase['type'],
							"groups" => $phase['groups']
						)
					));
				}
				else json_fail("Phase ".$phase_id." not found.");
			}
			else json_fail("Category ".$cat_id." not found.");
		}
		else json_fail("Category id is missing, please retry your request.");
	}

	//
	// CHANGE THE NAME OF A PHASE
	//
	function change_phase_name()
	{
		$phase_id = trim(strip_tags($_POST['phase_id']));
		$new_name = trim(strip_tags($_POST['new_name']));
		$cat_id   = trim(strip_tags($_POST['cat_id']));
		if(isset($phase_id) && isset($new_name) && isset($cat_id))
		{
			// Find the category
			$category_found = false;
			$phase_found = false;
			for ($i=0; $i<count($_SESSION['competition']['categories']); $i++)
			{
				if ($_SESSION['competition']['categories'][$i]['uniqid'] === $cat_id)
				{
					// Now find the phase
					for ($j=0; $j<count($_SESSION['competition']['categories'][$i]['phases']); $j++)
					{
						if ($_SESSION['competition']['categories'][$i]['phases'][$j]['uniqid'] === $phase_id)
						{
							$_SESSION['competition']['categories'][$i]['phases'][$j]['name'] = $new_name;
							$phase_found = true;
							break;
						}
					}
					$category_found = true;
					break;
				}
			}
			if ($category_found) 
			{
				if ($phase_found) 
					echo json_encode(array("res" => "ok"));
				else 
					json_fail("Phase ".$phase_id." not found.");
			}
			else json_fail("Category ".$cat_id." not found.");
		}
		else json_fail("At least one name is missing");
	}

	//
	// CHANGE THE NAME OF A PHASE
	//
	function change_phase_type()
	{
		$phase_id = trim(strip_tags($_POST['phase_id']));
		$new_type = trim(strip_tags($_POST['new_type']));
		$cat_id   = trim(strip_tags($_POST['cat_id']));
		if(isset($phase_id) && isset($new_type) && isset($cat_id))
		{
			// Find the category
			$category_found = false;
			$phase_found = false;
			for ($i=0; $i<count($_SESSION['competition']['categories']); $i++)
			{
				if ($_SESSION['competition']['categories'][$i]['uniqid'] === $cat_id)
				{
					// Now find the phase
					for ($j=0; $j<count($_SESSION['competition']['categories'][$i]['phases']); $j++)
					{
						if ($_SESSION['competition']['categories'][$i]['phases'][$j]['uniqid'] === $phase_id)
						{
							$_SESSION['competition']['categories'][$i]['phases'][$j]['type'] = intval($new_type);
							$phase_found = true;
							break;
						}
					}
					$category_found = true;
					break;
				}
			}
			if ($category_found) 
			{
				if ($phase_found) 
					echo json_encode(array("res" => "ok"));
				else 
					json_fail("Phase ".$phase_id." not found.");
			}
			else json_fail("Category ".$cat_id." not found.");
		}
		else json_fail("At least one name is missing");
	}

	// 
	// ADD A PHASE TO A CATEGORY
	//
	function add_phase()
	{
		if(isset($_POST['phase_name']) && isset($_POST['category']))
		{
			$phase_name = trim(strip_tags($_POST['phase_name']));
			$category = trim(strip_tags($_POST['category']));

			if (!empty($phase_name) && !empty($category))
			{
				$new_phase = create_phase($phase_name);

				// Find the category
				$categoryFound = false;
				for ($i=0; $i<count($_SESSION['competition']['categories']); $i++)
				{
					if ($_SESSION['competition']['categories'][$i]['uniqid'] === $category)
					{
						array_push($_SESSION['competition']['categories'][$i]['phases'], $new_phase);
						$categoryFound = true;
						break;
					}
				}
				if ($categoryFound) 
					echo json_encode(array("res" => "ok"));
				else 
					json_fail("Category ".$category." not found.");
				
			}
			else json_fail("The given category or phase name is empty or not valid. Please try again.");			
		
		}
		else json_fail("Phase id is missing, please retry your request.");

	}

	// 
	// SAVE THE PARAMETER : HIDE THE FIGHTERS IN A CATEGORY
	//
	function hide_fighters_in_category()
	{
		if (isset($_POST['value']))
		{
			$val = trim(strip_tags($_POST['value']));
			$_SESSION['hide_fighters_in_category'] = ($val === "1") ? true : false;

			echo json_encode(array("res" => "ok"));
		}
		else json_fail("Error : the value is missing. Can't decide to hide the fighters or not.");
	}

	// 
	// ADD A FIGHTER TO A CATEGORY
	//
	function add_fighter_to_category()
	{
		if(isset($_POST['fighter_id']) && isset($_POST['cat_id']))
		{
			$fighter_id = trim(strip_tags($_POST['fighter_id']));
			$cat_id = trim(strip_tags($_POST['cat_id']));

			if (!empty($fighter_id) && !empty($cat_id))
			{
				// Find the fighter
				$chosen_fighter = null;
				foreach($_SESSION['fighters'] as $f)
				{
					if ($f['uniqid'] === $fighter_id)
					{
						$chosen_fighter = $f;
						break;
					}
				}

				// Find the category
				if ($chosen_fighter != null)
				{
					// TODO : Verify that the fighter isn't already in the category

					$cat_found = false;
					for($i=0; $i<count($_SESSION['competition']['categories']); $i++)
					{
						if ($_SESSION['competition']['categories'][$i]['uniqid'] === $cat_id)
						{
							array_push($_SESSION['competition']['categories'][$i]['fighters'], $chosen_fighter);
							$cat_found = true;
							break;
						}
					}

					if ($cat_found) 
						echo json_encode(array("res" => "ok"));
					else 
						json_fail("Category ".$category." not found.");	
				}
				else json_fail("Fighter ".$fighter_id." not found.");	
			}
			else json_fail("Fighter id or category id is missing, please retry your request.");
		}
		else json_fail("Fighter id or category id is missing, please retry your request.");
	}

	// 
	// REMOVE A FIGHTER TO A CATEGORY
	//
	function remove_fighter_from_category()
	{
		if(isset($_POST['fighter_id']) && isset($_POST['cat_id']))
		{
			$fighter_id = trim(strip_tags($_POST['fighter_id']));
			$cat_id = trim(strip_tags($_POST['cat_id']));

			if (!empty($fighter_id) && !empty($cat_id))
			{
				// Get the category
				$cat_index = -1;
				for ($i=0; $i<count($_SESSION['competition']['categories']); $i++)
				{
					if ($_SESSION['competition']['categories'][$i]['uniqid'] === $cat_id)
					{
						$cat_index = $i;
						break;
					}
				}

				if ($cat_index != -1)
				{
					// Check if the fighter is inside the category's fighters
					$cat = $_SESSION['competition']['categories'][$cat_index];
					$f_removed = false;
					for ($i=0; $i<count($cat['fighters']); $i++)
					{
						if ($cat['fighters'][$i]['uniqid'] === $fighter_id)
						{
							array_splice($_SESSION['competition']['categories'][$cat_index]['fighters'], $i, 1);
							$f_removed = true;
							break;
						}
					}
					if ($f_removed) 
						echo json_encode(array("res" => "ok"));
					else
						json_fail("Unable to remove fighter : ".$fighter_id);
				}
				else json_fail("Category ".$category." not found.");
				
			}
			else json_fail("Fighter id or category id is missing, please retry your request.");
		}
		else json_fail("Fighter id or category id is missing, please retry your request.");

	}

	//
	// CREATE A POOL GROUP AND ITS MATCHES
	//
	function create_group_pool()
	{
		if(isset($_POST['phase_id']) && 
		   isset($_POST['cat_id']) && 
		   isset($_POST['group_name']) && 
		   isset($_POST['fighters_num']) && 
		   isset($_POST['pool_type']))
		{
			
			// Get the infos from the POST data
			$cat_id       = trim(strip_tags($_POST['cat_id']));
			$phase_id     = trim(strip_tags($_POST['phase_id']));
			$group_name   = trim(strip_tags($_POST['group_name']));
			$fighters_num = trim(strip_tags($_POST['fighters_num']));
			$pool_type    = trim(strip_tags($_POST['pool_type']));

			if (!empty($cat_id) && !empty($phase_id) && !empty($group_name) && !empty($fighters_num) && !empty($pool_type))
			{
				$category = &get_category($cat_id);
				$phase = &get_phase($phase_id, $category);

				// Create the group
				$group = create_group($group_name, $fighters_num);

				// Then create the matches
				for ($i=0; $i<$fighters_num-1; $i++)
				{
					for ($j=$i+1; $j<$fighters_num; $j++)
					{
						// Create the match
						$match = create_match($i, $j);
						array_push($group['matches'], $match);
					}
				}
				// Add the group to the phase
				array_push($phase['groups'], $group);

				echo json_encode(array(
					'res' => 'ok'
				));
			}
			else json_fail("At least one parameter is empty, please correct this and try again.");
		}
		else json_fail("Some parameters are missing, please retry your request.");
	}

	//
	// CREATE A BRACKET AND ALL THE GROUPS AND MATCHES IT INVOLVES
	//
	function create_bracket()
	{
		if(isset($_POST['phase_id']) && 
		   isset($_POST['cat_id']) && 
		   isset($_POST['bracket_start']) && 
		   isset($_POST['fight_all_places']))
		{
			
			// Get the infos from the POST data
			$cat_id           = trim(strip_tags($_POST['cat_id']));
			$phase_id         = trim(strip_tags($_POST['phase_id']));
			$bracket_start    = trim(strip_tags($_POST['bracket_start']));
			$fight_all_places = trim(strip_tags($_POST['fight_all_places']));

			if (!empty($cat_id) && 
				!empty($phase_id) && 
				!empty($bracket_start) && 
				!empty($fight_all_places))
			{
				$category = &get_category($cat_id);
				$phase = &get_phase($phase_id, $category);

				// Depending on the step we start at, we create the groups (and the matches) accordingly
				$start_val = intval($bracket_start);
				while($start_val > 1)
				{
					// Create the group
					$group = create_group("1/".$start_val." of finals", $start_val);

					// Then create the matches. On the 1/8th of finals, there are 4 matches, 
					// on the 1/4th 2 matches, and so on.
					for ($m=0; $m<$start_val/2; $m++)
					{						
						// Create the match : the nth meets the (max-n)th 
						// ie 0 against 8, 1 against 7, 2 against 6...
						$match = create_match($m, $start_val - $m);
						array_push($group['matches'], $match);
					}

					// Add the group to the phase
					array_push($phase['groups'], $group);

					$start_val /= 2;
				}

				echo json_encode(array(
					'res' => 'ok',
					'cat' => $category
				));
			}
			else json_fail("At least one parameter is empty, please correct this and try again.");
		}
		else json_fail("Some parameters are missing, please retry your request.");

	}

	//
	// Fill the groups of the given phase
	//
	function fill_groups()
	{

	}

	//
	// Empty the groups of the given phase
	//
	function empty_groups()
	{

	}

	//
	// WHEN AN ACTION IS FAILED, PUT THE FAIL CODE IN THE RESPONSE AND ADD A MESSAGE
	//
	function json_fail($msg)
	{
		echo json_encode(array(
			"res" => "fail",
			"errors" => array(
				0 => $msg
			)
		));	
	}

?>