<?php
	

?>
<div class="container">
	<div class="row">
		<h1>First step : load the fighters</h1>
		<div class="alert alert-secondary" role="alert">
			<p>The file to upload must be in csv format with the values separated by semicolons. The orders of the columns doesn't matter, the algorithm is able to load the data in any order.</p>
			<p>Here is an example of file :</p>
			<pre>last_name;first_name;license;birth_date;gender<br/>BRIGLIADOR;Gosselin;912741;24/01/81;m<br/>CHARTIER;Germain;617410;24/12/64;m<br/>AYOT;Léon;6511717;31/03/95;m<br/>...</pre>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		
		<form method="post" action="load_fighters.php" enctype="multipart/form-data">
			<div class="input-group">
				<div class="custom-file">
					<input type="file" class="custom-file-input" id="inputFightersFile" aria-describedby="btnUploadFighters" name="fighters_file">
					<!--input type="hidden" name="MAX_FILE_SIZE" value="2097152"-->
					<label class="custom-file-label" for="inputFightersFile">Choose file...</label>
				</div>
				<div class="input-group-append">
					<button class="btn btn-primary" type="submit" id="btnUploadFighters">Upload</button>
				</div>
			</div>
		</form>
		
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm">
			<?php
				if (isset($fighters))
				{
					for ($i=0; $i<round(count($fighters)/3); $i++)
					{
						$f = $fighters[$i];
						echo $f['last_name']." ".$f['first_name']." ".$f['license']." ".$f['birth_date']." ".$f['gender'] . '<br/>';
					}
				}
				else echo "No fighter loaded.";
			?>
		</div>

		<div class="col-sm">
			<?php
				if (isset($fighters))
				{
					for ($i=round(count($fighters)/3); $i<round(2*count($fighters)/3); $i++)
					{
						$f = $fighters[$i];
						echo $f['last_name']." ".$f['first_name']." ".$f['license']." ".$f['birth_date']." ".$f['gender'] . "<br/>";
					}
				}
			?>
		</div>

		<div class="col-sm">
			<?php
				if (isset($fighters))
				{
					for ($i=round(2*count($fighters)/3); $i<count($fighters); $i++)
					{
						$f = $fighters[$i];
						echo $f['last_name']." ".$f['first_name']." ".$f['license']." ".$f['birth_date']." ".$f['gender'] . "<br/>";
					}
				}
			?>
		</div>
	</div>
</div>

<div class="container pt-5">
	<div class="row">
		<div class="col-sm">
		</div>
		<div class="col-sm">
			<?php 
				if (isset($fighters))
					echo '<a class="btn btn-primary btn-lg" href="#" id="btn-start-competition" role="button">Next step : the competition</a>';
			?>
		</div>
		<div class="col-sm">
		</div>
	</div>
</div>
