<?php
	

?>
<div class="container">
	<div class="row">
		<div class="jumbotron">
			<h1 class="display-4">Welcome in Canager</h1>
			<p class="lead">This application will help you manage a competition, but only one at a time.</p>
			<hr class="my-4">
			<div class="alert alert-warning" role="alert">
  				For the moment, the application is in development, click on <a href="" id="home-todo-link" class="alert-link">the TODO menu</a> to see what has been done and what remains before the application is fully functional.
			</div>
			<p>It doesn't take care of the registration, but will help you manage pools, brackets, rankings, to save in a local file or a distant database.</p>
			<a class="btn btn-primary btn-lg" href="#" id="btn-start" role="button">Start !</a>
		</div>		
	</div>
</div>

