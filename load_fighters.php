﻿<?php
  session_start();

  print_r($_FILES['fighters_file']);

  if ($_FILES['fighters_file']['error'])
  {     
      switch ($_FILES['fighters_file']['error']){     
           case 1: // UPLOAD_ERR_INI_SIZE     
           echo"Le fichier dépasse la limite autorisée par le serveur (fichier php.ini) !";     
           break;     
           case 2: // UPLOAD_ERR_FORM_SIZE     
           echo "Le fichier dépasse la limite autorisée dans le formulaire HTML !"; 
           break;     
           case 3: // UPLOAD_ERR_PARTIAL     
           echo "L'envoi du fichier a été interrompu pendant le transfert !";     
           break;     
           case 4: // UPLOAD_ERR_NO_FILE     
           echo "Le fichier que vous avez envoyé a une taille nulle !"; 
           break;     
      }     
  }     
  else
  {     
   // $_FILES['fighters_file']['error'] vaut 0 soit UPLOAD_ERR_OK     
   // ce qui signifie qu'il n'y a eu aucune erreur     
    echo "Tout va bien, fichier ".$_FILES['fighters_file']['name']." bien reçu.";

    // Read the given file and put it, line by line, in an array
    $lines = file($_FILES['fighters_file']['name']);

    $fighters = array();
    foreach ($lines as $line_num => $line)
    {
      if ($line_num == 0) 
        $labels = explode(';', $lines[$line_num]);
      else
      {
        $fighter_values = explode(';', $lines[$line_num]);

        // Then create the associative array
        $fighter = array();
        for ($i=0; $i<count($labels); $i++)
          $fighter[trim($labels[$i])] = $fighter_values[$i];

        // Transform the birth date
        $birth_date = explode('-',$fighter['birth_date']);
        $timestamp = mktime(0,0,0, intval($birth_date[1]), intval($birth_date[0]), intval($birth_date[2]));

        $fighter['birth_date'] = date("Y-m-d", $timestamp);

        // Adding 2 fields that will be useful later
        $fighter['updated'] = false;
        $fighter['created'] = false;

        $fighter['uniqid'] = uniqid("f_");

        // Finally add the fighter in the fighters array
        array_push($fighters, $fighter);
      }
    }

    $_SESSION['fighters'] = $fighters;

    header('Location: index.php');
  }     
?>